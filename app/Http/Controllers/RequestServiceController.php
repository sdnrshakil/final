<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\RequestService;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class RequestServiceController extends Controller
{
    public function create()
    {




        $categories = Category::pluck('category_title', 'id')->toArray();
        return view('frontend.request_services.create', compact('categories'));
    }

    public function store(Request $request)
    {
        try {
            $request_servicesdata = $request->all();
            RequestService::create($request_servicesdata);
            return Redirect()->route('homepage');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }


    public function index()
    {

        $request_services = RequestService::orderBy('id', 'desc')->get();
        return view('frontend.request_services.index', compact('request_services'));
    }

    public function show($id)
    {


        $request_services = RequestService::where('id', $id)->firstOrFail();


        return view('frontend.request_services.show', compact('request_services'));
    }
}
