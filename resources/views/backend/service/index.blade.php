<x-backend.layouts.master>
    <h1 class="mt-4">Services</h1>


    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            <a class="btn btn-sm btn-info" href="">PDF</a>
            @can('product-trash-list')
            <a class="btn btn-sm btn-info" href="">Trash List</a>
            @endcan
            <a class="btn btn-sm btn-primary" href="{{route('service.create')}}">Add New</a>
        </div>
        <div class="card-body">

            @if(session('message'))
            <p class="alert alert-success">{{ session('message') }}</p>
            @endif

            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">

                <thead>
                    <tr>
                        <th>SL</th>
                        <th>service Title</th>
                        <th>Category</th>
                        <th>Rate</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>

                    @foreach ($services as $service)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $service->service_title }}</td>
                        <td>{{ $service->category }}</td>
                        <td>{{ $service->rate }}per hour</td>
                        
                        <td>

                         
                            <a class="btn btn-info btn-sm" href="{{route('service.show', [$service->id])}}">Show</a>    
                          

                            <a class="btn btn-warning btn-sm" href="">Edit</a>    
                            
                            <form action="" method="POST" 
                                style="display:inline">
                                @csrf
                                @method('delete')
                                <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure want to delete ?')">Delete</button>
                            </form>

                        </td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
</x-backend.layouts.master>