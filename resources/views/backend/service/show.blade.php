<x-backend.layouts.master>
    <h1 class="mt-4">Products</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
        <li class="breadcrumb-item active">Products</li>
    </ol>

    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            Product Details
            <a class="btn btn-sm btn-primary" href="{{ route('service.index') }}">List</a>
        </div>
        <div class="card-body">
            <h3>Category: {{ $service->category ?? 'N/A' }}</h3>
            <h3>Title: {{ $service->category_title }}</h3>
            <p>Description: {{ $service->description }}</p>
            <p>rate: {{ $service->rate }}</p>
            <img src="{{ asset('storage/services/'. $service->image) }}" />
           
        </div>

    </div>
</x-backend.layouts.master>