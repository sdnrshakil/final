<x-backend.layouts.master>
    <h1 class="mt-4">RequestServices</h1>


    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            <a class="btn btn-sm btn-info" href="">PDF</a>
            @can('product-trash-list')
            <a class="btn btn-sm btn-info" href="">Trash List</a>
            @endcan
            <a class="btn btn-sm btn-primary" href="{{route('request_services.create')}}">Add New</a>
        </div>
        <div class="card-body">

            @if(session('message'))
            <p class="alert alert-success">{{ session('message') }}</p>
            @endif

            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">

                <thead>
                    <tr>
                        <th>SL</th>
                        <th>Category</th>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Address</th>  
                        <th>Rate</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>

                    @foreach ($request_services as $request_service)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $request_service->category }}</td>
                        <td>{{ $request_service->name }}</td>
                        <td>{{ $request_service->phone_num}}</td>
                        <td>{{ $request_service->address}}</td>
                        <td>{{ $request_service->rate}}TK Per hour</td>
                        <td>

                         
                         <a class="btn btn-info btn-sm" href="{{route('request_services.show', [$request_service->id])}}">Show</a>    
                          
                            <a class="btn btn-warning btn-sm" href="">Edit</a>    
                            
                            <form action="" method="POST" 
                                style="display:inline">
                                @csrf
                                @method('delete')
                                <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure want to delete ?')">Delete</button>
                            </form>

                        </td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
</x-backend.layouts.master>